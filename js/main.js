// Initialization

var jsonData = null;

var storyData = {
    currentPassage: 0,
    currentPassageID: 0,   
};

// Loading
// $(window).on('load', function() {
//     function loading() {
//         $("#loader").fadeOut("slow");
//     };
//     window.setTimeout(loading, 5000); // 5 seconds
// })

// JSON Loading
function loadJSON(callback) {

    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
    xobj.open('GET', 'data/story.json', true);
    xobj.onreadystatechange = function() {
        if (xobj.readyState == 4 && xobj.status == 200) {

            // .open will NOT return a value but simply returns undefined in async mode so use a callback
            callback(xobj.responseText);

        }
    };
    xobj.send(null);

}

// Call to function with anonymous callback
loadJSON(function(response) {
    // console.log(response);
     jsonData = JSON.parse(response);
     for (var i = 0 ; i < jsonData.storyContent.length ; i++) {
        //  console.log(jsonData[i].text);
         jsonData.storyContent[i].text = jsonData.storyContent[i].text.replace(/\n/g, "<br>");
     }    
    initStory(jsonData);
});

function initStory(data)  {
    document.title = data.storyTitle;
    document.getElementById("story-title").innerHTML = data.storyTitle;
    document.getElementById("story-location").innerHTML = data.storyContent[storyData.currentPassage].location;
    document.getElementById("story-text").innerHTML = data.storyContent[storyData.currentPassage].text;
    document.getElementById("story-link").innerHTML = data.storyContent[storyData.currentPassage].choices[0].description;
}
